/*
 * @Author: ChaiHongJun
 * @Date: 2021-03-23 10:09:54
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-03-23 20:27:54
 * @version: 
 * @Description: 
 */
module.exports = {
  presets: ["@vue/cli-plugin-babel/preset"],
  plugins: [
    [
      "component",
      {
        libraryName: "element-ui",
        styleLibraryName: "theme-chalk"
      }
    ]
  ]
};
