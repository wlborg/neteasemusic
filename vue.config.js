/*
 * @Author: ChaiHongJun
 * @Date: 2021-03-25 17:08:06
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-03-25 17:08:34
 * @version: 
 * @Description: 
 */
module.exports = {
  css: {
    loaderOptions: {
      stylus: {
        import: "~@/assets/styles/variable.styl"
        // import: ["~@/assets/variable.styl", "~@/assets/variable2.styl"]
      }
    }
  }
}
