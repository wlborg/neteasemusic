/*
 * @Author: ChaiHongJun
 * @Date: 2021-04-01 09:19:13
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-05-03 10:48:53
 * @version:
 * @Description:全局使用的工具类函数
 */
// 格式化播放量
export function formatPlayCount(number) {
  let stringNumber = number.toString();
  let numberLength = stringNumber.length;
  //亿级别
  if (numberLength > 8) {
    return parseInt(number / 100000000) + "亿";
  }
  // 万级别
  else if (numberLength > 4) {
    return parseInt(number / 10000) + "万";
  } else {
    return number;
  }
}
// 时间格式化  XXXX年-XX月-XX日
export function formatTimeStamp(timestamp) {
  let now = new Date(timestamp);
  let year = now.getFullYear(); //取得4位数的年份
  let month = now.getMonth() + 1; //取得日期中的月份，其中0表示1月，11表示12月
  let date = now.getDate(); //返回日期月份中的天数（1到31）
  // let hour = now.getHours();     //返回日期中的小时数（0到23）
  // let minute = now.getMinutes(); //返回日期中的分钟数（0到59）
  // let second = now.getSeconds(); //返回日期中的秒数（0到59）
  return year + "-" + month + "-" + date;
}
//时间戳转化成时分秒
export function formatDuration(timestamp) {
  let time;
  let hours = parseInt((timestamp % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  let minutes = parseInt((timestamp % (1000 * 60 * 60)) / (1000 * 60));
  let seconds = (timestamp % (1000 * 60)) / 1000;
  time = (hours < 10 ? ('0' + hours) : hours) + ':' + (minutes < 10 ? ('0' + minutes) : minutes) + ':' + (seconds < 10 ? ('0' + seconds) : seconds);
  return time;
}
// 将毫秒转化为XX分XX秒
export function formatSongDuration(millisecond) {
  let second = Math.round(millisecond / 1000);
  let min = Math.trunc(second / 60);
  let sec = second % 60;
  if (min < 10) {
    min = "0" + min;
  }
  if (sec < 10) {
    sec = "0" + sec;
  }
  return min + ":" + sec;
}
// js时间转化为几天前,几小时前，几分钟前
export function getDateDiff(timestamp) {
  let result = "";
  let minute = 1000 * 60;
  let hour = minute * 60;
  let day = hour * 24;
  let month = day * 30;
  let now = new Date().getTime();
  let diffValue = now - timestamp;
  if (diffValue < 0) return;
  let monthC = diffValue / month;
  let weekC = diffValue / (7 * day);
  let dayC = diffValue / day;
  let hourC = diffValue / hour;
  let minC = diffValue / minute;
  if (monthC >= 1) {
    result = "" + parseInt(monthC) + "月前";
  } else if (weekC >= 1) {
    result = "" + parseInt(weekC) + "周前";
  } else if (dayC >= 1) {
    result = "" + parseInt(dayC) + "天前";
  } else if (hourC >= 1) {
    result = "" + parseInt(hourC) + "小时前";
  } else if (minC >= 1) {
    result = "" + parseInt(minC) + "分钟前";
  } else result = "刚刚";
  return result;
}
// 获取当前滚动条高度
export function getScrollTop() {
  let scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
  return scrollTop;
}
// 获取当前可视区域高度
export function getClientTop() {
  let clientHeight = document.documentElement.clientHeight || document.body.clientHeight;
  return clientHeight;
}
//获取页面整个高度
export function getScrollHeight() {
  let scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight;
  return scrollHeight;
}
//获取当前的时间戳
export function getTimestamp() {
  return new Date().valueOf();
}
//将个位数转化为两位
export function covetDigitToDouble(digit) {
  if (digit < 10) {
    return "0" + digit;
  } else {
    return digit;
  }
}
//生成指定范围内的随机数
export function generatingRandom(minimum, maximum, length) {
  let bool = true;
  while (bool) {
    var number = (Math.floor(Math.random() * maximum + 1) + minimum);
    if (number > length) {
      bool = true;
    } else {
      bool = false;
    }
  }
  return number;
}
export function fixAudioPlay(item) {
  var thePromise = item.play();
  if (thePromise != undefined) {
    thePromise.then(function () {
      item.pause();
      item.currentTime = 0;
    });
  }
}

// 获取指定歌曲ID的播放地址
export function getTrackSrc(id) {
  return `https://music.163.com/song/media/outer/url?id=${id}.mp3`;
}


//转义菜单
export function URLencode(sStr) {
  return sStr.replace(/\&/g, "%26").replace(/\//g, "%2F");
}