/*
 * @Author: ChaiHongJun
 * @Date: 2021-03-23 17:35:11
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-05-03 11:26:16
 * @version:
 * @Description:
 */
export const routes = [
  {
    path: "",
    name: "Home",
    component: () => import("@/views/home/Index.vue"),
  },
  {
    path: "/rank",
    name: "Rank",
    component: () => import("@/views/rank/Index.vue"),
  },
  {
    path: "/playlist",
    name: "Playlist",
    component: () => import("@/views/playlist/Index.vue"),
  },
  {
    path: "/playlist/detail",
    name: "Detail",
    component: () => import("@/views/playlist/detail/Index.vue"),
  },
  {
    path: "/singers",
    name: "Singers",
    component: () => import("@/views/singers/Index.vue"),
  },
  {
    path: "/mv",
    name: "MV",
    component: () => import("@/views/mv/Index.vue"),
  },
  {
    path: "/mv/detail",
    name: "mvDetail",
    component: () => import("@/views/mv/detail/Index.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/login/Index.vue"),
  },
]