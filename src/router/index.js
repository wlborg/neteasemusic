/*
 * @Author: ChaiHongJun
 * @Date: 2021-03-23 10:09:54
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-04-09 10:54:32
 * @version: 
 * @Description: 
 */
import Vue from "vue";
import VueRouter from "vue-router";
import { routes } from "./routes.js";
// const originalReplace = VueRouter.prototype.replace;
// VueRouter.prototype.replace = function replace(location) {
//   return originalReplace.call(this, location).catch(err => err);
// };
Vue.use(VueRouter);
const router = new VueRouter({
  routes,
});
export default router;
