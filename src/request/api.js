/*
 * @Author: ChaiHongJun
 * @Date: 2021-03-23 21:11:44
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-05-01 17:26:38
 * @version:
 * @Description:API请求管理
 */
import api from "./axios";
import { getTimestamp } from "@/utils/index.js";  // 时间戳
//获取banner
// https://neteasecloudmusicapi.vercel.app/#/?id=banner
export const getBanner = () => api.get("/banner" + "?timestamp=" + getTimestamp());
//推荐歌单
// https://neteasecloudmusicapi.vercel.app/#/?id=%e6%8e%a8%e8%8d%90%e6%ad%8c%e5%8d%95
export const getPersonalized = (limit = 30) => api.get("/personalized?limit=" + limit + "&timestamp=" + getTimestamp());
//推荐新歌
// https://neteasecloudmusicapi.vercel.app/#/?id=%e6%8e%a8%e8%8d%90%e6%96%b0%e9%9f%b3%e4%b9%90
export const getNewSongs = (limit = 10) =>
  api.get(
    "/personalized/newsong?limit=" + limit + "&timestamp=" + getTimestamp()
  );
//推荐歌手
// https://neteasecloudmusicapi.vercel.app/#/?id=%e7%83%ad%e9%97%a8%e6%ad%8c%e6%89%8b
export const getArtists = (offset = 0, limit = 50) =>
  api.get(
    "/top/artists?offset=" +
    offset +
    "&limit=" +
    limit +
    "&timestamp=" +
    getTimestamp()
  );
//获取各种榜单
// https://neteasecloudmusicapi.vercel.app/#/?id=%e6%89%80%e6%9c%89%e6%a6%9c%e5%8d%95%e5%86%85%e5%ae%b9%e6%91%98%e8%a6%81
export const getTop = () => api.get("/toplist/detail" + "?timestamp=" + getTimestamp())
//获取歌单分类
// https://neteasecloudmusicapi.vercel.app/#/?id=%e6%ad%8c%e5%8d%95%e5%88%86%e7%b1%bb
export const getCategories = () => api.get("/playlist/catlist" + "?timestamp=" + getTimestamp())
//获取热门歌单分类
// https://neteasecloudmusicapi.vercel.app/#/?id=%e7%83%ad%e9%97%a8%e6%ad%8c%e5%8d%95%e5%88%86%e7%b1%bb
export const getTags = () => api.get("/playlist/hot" + "?timestamp=" + getTimestamp())
// 热门歌单
// pageSize 每页歌单数量
// offset  偏移量（分页）
// order  hot/new
// cat    全部/华语/... 
// https://neteasecloudmusicapi.vercel.app/#/?id=%e6%ad%8c%e5%8d%95-%e7%bd%91%e5%8f%8b%e7%b2%be%e9%80%89%e7%a2%9f-
export const getChosenPlaylist = (pageSize = 50, offset = 0, order = 'hot', cat = '全部') => api.get("/top/playlist?limit=" + pageSize + "&offset=" + offset + "&order=" + order + "&cat=" + cat + "&timestamp=" + getTimestamp())
// 获取歌单详情
// id  歌单id
// subscriber 订阅者数量
export const getPlaylistDetail = (id, subscriber = 8) => api.get("/playlist/detail?id=" + id + "&s=" + subscriber + "&timestamp=" + getTimestamp())
//获取歌手分类
/**
 *  limit 每页数量 默认 30
 *  offset 偏移量 (页数-1)*limit 
 *  type -1 全部  1 男 2 女  3  乐队
 *  area -1 全部  7华语 96 欧美  8  日本  16 韩国  0  其他
 *  initial 首字母查询
 */
export const getArtistList = (limit = 30, offset = 0, area = -1, type = -1, initial = -1) => api.get("/artist/list?limit=" + limit + "&offset=" + offset + "&area=" + area + "&type=" + type + "&initial=" + initial + "&timestamp=" + getTimestamp())
//相关歌单推荐
// id 歌单id
export const getRelatedPlaylist = (id) => api.get("/related/playlist?id=" + id + "&timestamp=" + getTimestamp());
//歌单评论
// id 歌单id
// limit  评论数
export const getPlaylistComments = (id, limit = 20) => api.get("/comment/playlist?id=" + id + "&limit=" + limit + "&timestamp=" + getTimestamp());
// 获取MV数据
// limit 每页数据  默认 30
// offset 偏移量 (页数-1)*limit
// area 全部,内地,港台,欧美,日本,韩国,不填则为全部
// type 为全部,官方版,原生,现场版,网易出品,不填则为全部
// order  可选值为上升最快,最热,最新,不填则为上升最快
export const getMVList = (limit = 30, offset, area, type, order) => api.get("/mv/all?limit=" + limit + "&offset=" + offset + "&area=" + area + "&type=" + type + "&order=" + order + "&timestamp=" + getTimestamp());
// 获取MV 地址
// id 视频id
export const getMvUrl = (id) => api.get("/mv/url?id=" + id + "&timestamp=" + getTimestamp())
// 获取mv 全部评论
// id  视频id
// limit 每页评论数量
// offset 偏移量 (页数-1)*limit
export const getMvComments = (id, limit, offset) => api.get("/comment/mv?id=" + id + "&limit=" + limit + "&offset=" + offset + "&timestamp=" + getTimestamp())
// 获取mv 热门评论
// id  视频id
// export const getHotComments = (id) => api.get("/comment/mv?id=" + id + "&timestamp=" + getTimestamp())
//相似MV
// id  视频id
export const getSimiMvs = (id) => api.get("/simi/mv?mvid=" + id + "&timestamp=" + getTimestamp())
// mv 详情数据
//id 视频id
export const getMvDetail = (id) => api.get("/mv/detail?mvid=" + id + "&timestamp=" + getTimestamp())

// MV 点赞 转发等
export const getMvSocial = (id) => api.get("mv/detail/info?mvid=" + id + "&timestamp=" + getTimestamp())

//获取音乐URL
// id 音乐id,如果要一次性获得多个音乐iD，则用逗号分开
// 传入的ID是单个值，或者是一个数组
export const getMusicUrl = (id) => {
  if (Array.isArray(id)) {
    let str = "";
    id.forEach((item) => (str += item + ","));
    return api.get("/song/url?id=" + str.substr(0, str.length - 1));
  } else {
    return api.get("/song/url?id=" + id);
  }
}

/*   歌曲详情   */
// id 是单个ID 或者是一个数组
// export const getMusicDetail = (id) => {
//   if (Array.isArray(id)) {
//     let str = "";
//     id.forEach((item) => (str += item + ","));
//     return api.post("/song/detail?ids=" + str.substr(0, str.length - 1) + "$timestamp=" + (new Date()).valueOf());
//   } else {
//     return api.post("/song/detail?ids=" + id + "$timestamp=" + (new Date()).valueOf());
//   }
// }

/*   歌曲详情   */
// id   "ids=1234,675677,8979,3455,678"
export const getPlaylistTracks = (ids) => api.post("/song/detail?timestamp=" + getTimestamp(), { ids: ids });
