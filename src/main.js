/*
 * @Author: ChaiHongJun
 * @Date: 2021-03-23 10:09:54
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-04-13 18:07:11
 * @version: 
 * @Description: 
 */
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
Vue.config.productionTip = false;
import "@/assets/styles/reset.styl";

import { Image, Loading, Pagination, Progress, Slider } from "element-ui";
import "element-ui/lib/theme-chalk/index.css"
import "@/assets/styles/common.css";
import "@/assets/styles/iconfont.css";
Vue.use(Image);
Vue.use(Loading);
Vue.use(Pagination);
Vue.use(Progress);
Vue.use(Slider);
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
