/*
 * @Author: ChaiHongJun
 * @Date: 2021-04-10 11:22:33
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-05-01 17:23:13
 * @version:
 * @Description:相当于计算属性
 */
const getters = {
  /* 首页 */
  // banner
  getBanner: (state) => state.banner,
  //推荐歌单
  getPersonalized: (state) => state.personalized,
  //推荐新歌
  getNewSongs: (state) => state.newSongs,
  // 推荐歌手
  getArtists: (state) => state.artists,
  /* 排行榜 */
  getTop: (state) => state.list,

  /* 歌单 */
  // 热门标签
  getTags: (state) => state.tags,

  // 歌单大分类
  getCategories: (state) => state.categories,
  // 歌单小分类
  getSub: (state) => state.sub,
  // 精选歌单
  getChosenPlaylist: (state) => state.chosenPlaylist,
  // 精选歌单数量
  getChosenPlaylistCount: (state) => state.chosenPlaylistCount,

  /* 歌手 */
  getArtistList: (state) => state.artistList,

  /* MV */
  getMvList: (state) => state.mvList,
  // MV 播放地址
  getMvSrc: (state) => state.mvSrc,
  // MV 热门评论
  getMvHotComments: (state) => state.mvHotComments,
  // MV 最新评论
  getMvComments: (state) => state.mvComments,
  // MV 评论数
  getMvCommentsCount: (state) => state.mvCommentsCount,
  // 相关mv
  getSimiMvs: (state) => state.simiMvs,
  // MV 详情（不含地址）
  getMvDetail: (state) => state.mvDetail,
  // MV 点赞数
  getMvLikedCount: (state) => state.mvLikedCount,
  // MV 分享数
  getMvShareCount: (state) => state.mvShareCount,



  //歌单全部歌曲
  getPlaylistDetail: (state) => state.playlistDetail,
  // 歌单歌曲ID
  getTrackIds: (state) => state.trackIds,
  // 歌单歌曲
  getSongs: (state) => state.songs,
  // 相关歌单
  getRelatedPlaylist: (state) => state.relatedPlaylist,
  // 歌单评论
  getPlaylistComments: (state) => state.playlistComments,

  //  显示播放器
  getShowPlayer: (state) => state.showPlayer,

  // 显示历史播放列表
  getShowHistoryQueue: (state) => state.showHistoryQueue,


  // 播放状态
  getPlayStatus: (state) => state.playStatus,
  // 播放模式 0 循环  1单曲  2随机
  getPlayMode: (state) => state.playMode,
  // 是否静音 true 是  false 否
  getMuted: (state) => state.isMuted,

  // 播放队列
  getPlayingQueue: (state) => state.playingQueue,

  // 历史播放队列
  getHistoryQueue: (state) => state.historyQueue,

  // 获取当前 播放列表歌曲索引
  getPlayingIndex: (state) => state.playingIndex,
  ///////////////////////////////////////////////////////////////////




  // 当前播放歌曲
  getPlayingTrack: (state) => state.playingTrack,

  // 播放的歌曲列表
  getPlaylist: (state) => state.playlist,
  // 播放歌曲数量
  getPlaylistLength: (state) => state.playlist.length,
  // 播放状态 播放 or 暂停



}
export default getters;
