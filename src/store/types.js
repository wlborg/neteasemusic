/*
 * @Author: ChaiHongJun
 * @Date: 2021-04-10 11:23:05
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-05-01 17:19:22
 * @version:
 * @Description:配置 types 匹配 action 和 mutations
 */

/* 首页 */
//banner
export const SHOW_BANNER = "SHOW_BANNER";
// 推荐歌单
export const GET_RECOMMEND_PLAYLIST = "GET_RECOMMEND_PLAYLIST";
// 推荐新歌
export const GET_RECOMMEND_NEWSONGS = "GET_RECOMMEND_NEWSONGS";
// 推荐歌手
export const GET_RECOMMEND_ARTISTS = "GET_RECOMMEND_ARTISTS";

/*  排行榜 */
export const GET_TOP = "GET_TOP";

/* 歌单 */
// 热门标签
export const GET_HOT_TAGS = "GET_HOT_TAGS";
//歌单大分类
export const GET_CATEGORIES = "GET_CATEGORIES";
//歌单小分类
export const GET_SUB = "GET_SUB";
// 精选歌单
export const GET_CHOSEN_PLAYLIST = "GET_CHOSEN_PLAYLIST";
//精选歌单数量
export const GET_CHOSEN_PLAYLIST_COUNT = "GET_CHOSEN_PLAYLIST_COUNT";

/* 歌手  */
// 默认歌手列表
export const GET_ARTIST_LIST = "GET_ARTIST_LIST";


/* MV  */
// 默认MV列表
export const GET_MV_LIST = "GET_MV_LIST";
//  获取MV播放地址 */
export const GET_MV_SRC = "GET_MV_SRC";
//* 清空MV列表 */
export const REMOVE_MV_LIST = "REMOVE_MV_LIST";

//* MV 热门评论 */
export const GET_MV_HOTCOMMENTS = "GET_MV_HOTCOMMENTS";

//* MV 最新评论 */
export const GET_MV_COMMENTS = "GET_MV_COMMENTS";

//* MV 评论数 */
export const GET_MV_COMMENTS_COUNT = "GET_MV_COMMENTS_COUNT";

//* 相关 MV  */
export const GET_SIMI_MV = "GET_SIMI_MV";

//MV 详情
export const GET_MV_DETAIL = "GET_MV_DETAIL";
// MV 点赞数
export const GET_MV_LIKED_COUNT = "GET_MV_LIKED_COUNT";
// MV 分享数
export const GET_MV_SHARE_COUNT = "GET_MV_SHARE_COUNT";

//获取歌单歌曲
export const GET_PLAYLIST_DETAIL = "GET_PLAYLIST_DETAIL";
// 歌单歌曲ID
export const GET_PLAYLIST_TRACKID = "GET_PLAYLIST_TRACKID";
// 歌单歌曲
export const GET_PLAYLIST_TRACK = "GET_PLAYLIST_TRACK";
// 获取相关歌单
export const GET_RELATED_PLAYLIST = "GET_RELATED_PLAYLIST";

// 歌单评论
export const GET_PLAYLIST_COMMENTS = "GET_PLAYLIST_COMMENTS";


// 显示播放器
export const SHOW_PLAYER = "SHOW_PLAYER";

// 播放或暂停
export const PLAY_OR_PAUSE = "PLAY_OR_PAUSE";

//播放模式
export const SET_PLAYMODE = "SET_PLAYMODE";

// 静音状态
export const SET_MUTE = "SET_MUTE";

// 播放队列
export const SET_PLAYING_QUEUE = "SET_PLAYING_QUEUE";

// 显示历史播放列表
export const SET_HISTORY_QUEUE = "SET_HISTORY_QUEUE";
//////////////////////


//添加歌单全部到待播列表
export const ADD_TO_PLAYLIST = "ADD_TO_PLAYLIST";



//当前播放歌曲
export const PLAYING_TRACK = "PLAYING_TRACK";

//已经播放歌曲添加到历史播放列表
export const SET_HISTORY_PLAYLIST = "SET_HISTORY_PLAYLIST";



//清空历史播放列表
export const CLEARALL_HISTORY_PLAYLIST = "CLEARALL_HISTORY_PLAYLIST";

// 删除某条播放历史记录
export const REMOVE_FROM_HISTORY = "REMOVE_FROM_HISTORY";



// 当前播放的歌曲索引
export const SET_TRACK_INDEX = "SET_TRACK_INDEX";
