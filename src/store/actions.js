/*
 * @Author: ChaiHongJun
 * @Date: 2021-04-09 16:25:32
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-05-01 17:18:09
 * @version:
 * @Description: 异步获取操作
 */
//  showPlaylistDetail 获取歌单详情
import {
  getBanner,
  getPersonalized,
  getNewSongs,
  getArtists,
  getTop,
  getTags,
  getCategories,
  getChosenPlaylist,
  getArtistList,
  getMVList,
  getMvUrl,
  getMvComments,
  getSimiMvs,
  getMvDetail,
  getMvSocial,
  getPlaylistDetail,
  getRelatedPlaylist,
  getPlaylistComments,
  getPlaylistTracks,
} from "@/request/api.js";
const actions = {
  /* 首页 */
  // 加载banner
  getBanner({ commit }) {
    getBanner()
      .then((res) => {
        if (res.code == 200) {
          commit("SHOW_BANNER", res.banners);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      });
  },
  //  首页推荐歌单
  getPersonalized({ commit }, limit) {
    getPersonalized(limit)
      .then((res) => {
        if (res.code == 200) {
          commit("GET_RECOMMEND_PLAYLIST", res.result);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      });
  },
  // 首页 推荐新歌
  getNewSongs({ commit }, limit) {
    getNewSongs(limit)
      .then((res) => {
        if (res.code == 200) {
          commit("GET_RECOMMEND_NEWSONGS", res.result);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      });
  },
  // 首页 推荐歌手
  getArtists({ commit }, { offset, limit }) {
    getArtists(offset, limit)
      .then((res) => {
        if (res.code == 200) {
          commit("GET_RECOMMEND_ARTISTS", res.artists);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      });
  },
  /*  排行榜 */
  getTop({ commit }) {
    getTop()
      .then((res) => {
        if (res.code == 200) {
          commit("GET_TOP", res.list);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      });
  },
  /* 歌单 */
  // 热门标签
  getTags({ commit }) {
    getTags()
      .then((res) => {
        if (res.code == 200) {
          commit("GET_HOT_TAGS", res.tags);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      });
  },
  //  歌单分类 导航
  getCategories({ commit }) {
    getCategories()
      .then((res) => {
        if (res.code == 200) {
          commit("GET_CATEGORIES", res.categories);
          commit("GET_SUB", res.sub);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      });
  },
  //精选歌单 （默认）
  getChosenPlaylist({ commit }, { pageSize, offset, order, cat }) {
    getChosenPlaylist(pageSize, offset, order, cat)
      .then((res) => {
        commit("GET_CHOSEN_PLAYLIST", res.playlists);
        commit("GET_CHOSEN_PLAYLIST_COUNT", res.total);
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      });
  },
  /*  歌手 */
  getArtistList({ commit }, { limit, offset, area, type, initial }) {
    getArtistList(limit, offset, area, type, initial)
      .then((res) => {
        if (res.code == 200) {
          commit("GET_ARTIST_LIST", res.artists);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      });
  },
  /*  MV  */
  getMVList({ commit }, { limit, offset, area, type, order }) {
    getMVList(limit, offset, area, type, order)
      .then((res) => {
        if (res.code == 200) {
          commit("GET_MV_LIST", res.data);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      });
  },
  // 获取MV 播放URL
  getMvUrl({ commit }, id) {
    getMvUrl(id)
      .then((res) => {
        if (res.code == 200) {
          commit("GET_MV_SRC", res.data.url);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      })
  },
  // 获取MV 热门论
  getHotComments({ commit }, { id, limit, offset }) {
    getMvComments(id, limit, offset)
      .then((res) => {
        if (res.code == 200) {
          commit("GET_MV_HOTCOMMENTS", res.hotComments);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      })
  },
  // 获取MV全部评论 和 评论数
  getMvComments({ commit }, { id, limit, offset }) {
    getMvComments(id, limit, offset)
      .then((res) => {
        if (res.code == 200) {
          commit("GET_MV_COMMENTS", res.comments);
          commit("GET_MV_COMMENTS_COUNT", res.total);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      })
  },
  // 相关MV
  getSimiMvs({ commit }, id) {
    getSimiMvs(id)
      .then((res) => {
        if (res.code == 200) {
          commit("GET_SIMI_MV", res.mvs);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      })
  },
  // MV 详情（不含播放地址）
  getMvDetail({ commit }, id) {
    getMvDetail(id)
      .then((res) => {
        if (res.code == 200) {
          commit("GET_MV_DETAIL", res.data);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      })

  },
  // MV 点赞 转发等数
  getMvSocial({ commit }, id) {
    getMvSocial(id)
      .then((res) => {
        if (res.code == 200) {
          commit("GET_MV_LIKED_COUNT", res.likedCount);
          commit("GET_MV_SHARE_COUNT", res.shareCount);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      })
  },


  /* 显示播放器 */
  showPlayer({ commit }, status) {
    commit("SHOW_PLAYER", status);
  },
  /* 播放歌曲 */
  playSingleTrack({ commit, getters }, track) {
    console.log('播放单曲：');
    //判断 设置 播放 状态  播放  or 暂停
    if (!getters.getPlayStatus) {
      commit("PLAY_OR_PAUSE", true);
    }
    // 设置播放 模式?
    // 将播放歌曲放入播放队列
    if (!getters.getPlayingQueue.indexOf(track)) {
      commit("SET_PLAYING_QUEUE", track);
    }
  },
  // 加载歌单详情
  // id 歌单ID
  //  subscribersCount 订阅人数
  showPlaylistDetail({ commit, getters }, { id, subscribersCount }) {
    getPlaylistDetail(id, subscribersCount)
      .then((res) => {
        if (res.code == 200) {
          // 获取歌单信息
          commit("GET_PLAYLIST_DETAIL", res.playlist);
          // console.log(this.getPlaylistDetail);
          // 获取歌单歌曲ID
          getters.getPlaylistDetail.trackIds.forEach((item) => {
            getters.getTrackIds.push(item.id);
          });
          //再获取歌单对应的歌曲信息
          getPlaylistTracks(getters.getTrackIds.join(","))
            .then((res) => {
              if (res.code == 200) {
                commit("GET_PLAYLIST_TRACK", res.songs);
              }
              // 添加歌曲播放URL
              getters.getSongs.forEach((item) => {
                item.audioSrc =
                  "https://music.163.com/song/media/outer/url?id=" +
                  item.id +
                  ".mp3";
              });
              console.log("歌曲详情：");
              console.log(getters.getSongs);
            })
            .catch((err) => {
              console.log("err:");
              console.log(err);
            });
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      });
  },
  // 加载相关歌单
  // id 歌单ID
  showRelatedPlaylist({ commit }, id) {
    getRelatedPlaylist(id)
      .then((res) => {
        if (res.code == 200) {
          commit("GET_RELATED_PLAYLIST", res.playlists);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      });
  },
  // 加载歌单评论
  // id 歌单ID
  // commentNum 歌单评论数
  showPlaylistComments({ commit }, { id, commentNum }) {
    getPlaylistComments(id, commentNum)
      .then((res) => {
        if (res.code == 200) {
          commit("GET_PLAYLIST_COMMENTS", res.hotComments);
        }
      })
      .catch((err) => {
        console.log("err:");
        console.log(err);
      })
  }
}
export default actions