/*
 * @Author: ChaiHongJun
 * @Date: 2021-03-23 10:09:54
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-04-10 11:35:00
 * @version: 
 * @Description: 状态管理 公共出口
 */
import Vue from "vue";
import Vuex from "vuex";
import state from "./state.js";
import mutations from "./mutations.js";
import getters from "./getters.js";
import actions from "./actions.js";
Vue.use(Vuex);

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
});
