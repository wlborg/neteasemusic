/*
 * @Author: ChaiHongJun
 * @Date: 2021-04-09 16:25:23
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-05-01 17:21:05
 * @version:
 * @Description:
 */
import * as TYPES from "./types.js"
const mutations = {
  /* 首页 */
  // 加载Banner
  [TYPES.SHOW_BANNER](state, banner) {
    state.banner = banner;
  },
  // 推荐歌单
  [TYPES.GET_RECOMMEND_PLAYLIST](state, personalized) {
    state.personalized = personalized;
  },
  // 推荐新歌
  [TYPES.GET_RECOMMEND_NEWSONGS](state, newSongs) {
    state.newSongs = newSongs;
  },
  // 推荐歌手
  [TYPES.GET_RECOMMEND_ARTISTS](state, artists) {
    state.artists = artists;
  },
  /* 排行榜 */
  [TYPES.GET_TOP](state, list) {
    state.list = list;
  },
  /* 歌单 */
  // 热门标签
  [TYPES.GET_HOT_TAGS](state, tags) {
    state.tags = tags;
  },
  // 歌单大分类
  [TYPES.GET_CATEGORIES](state, categories) {
    state.categories = categories;
  },
  // 歌单小分类
  [TYPES.GET_SUB](state, sub) {
    state.sub = sub;
  },
  // 默认精选歌单
  [TYPES.GET_CHOSEN_PLAYLIST](state, chosenPlaylist) {
    state.chosenPlaylist = chosenPlaylist;
  },
  // 默认精选歌单数量
  [TYPES.GET_CHOSEN_PLAYLIST_COUNT](state, chosenPlaylistCount) {
    state.chosenPlaylistCount = chosenPlaylistCount;
  },
  /* 歌手  */
  [TYPES.GET_ARTIST_LIST](state, artistList) {
    state.artistList.push(...artistList);
  },

  /*  MV  */
  [TYPES.GET_MV_LIST](state, mvList) {
    state.mvList.push(...mvList);
  },
  /* 获取MV 播放UR了 */
  [TYPES.GET_MV_SRC](state, mvSrc) {
    state.mvSrc = mvSrc;
  },

  /* 清空MV列表  */
  [TYPES.REMOVE_MV_LIST](state) {
    state.mvList = [];
  },

  /* MV热门评论  */
  [TYPES.GET_MV_HOTCOMMENTS](state, mvHotComments) {
    state.mvHotComments = mvHotComments;
  },

  /* MV 最新评论  */
  [TYPES.GET_MV_COMMENTS](state, mvComments) {
    state.mvComments = mvComments;
  },

  /* MV 评论数  */
  [TYPES.GET_MV_COMMENTS_COUNT](state, count) {
    state.mvCommentsCount = count;
  },
  /* 相关MV  */
  [TYPES.GET_SIMI_MV](state, mvs) {
    state.simiMvs = mvs;
  },
  // MV详情 (不含播放地址)
  [TYPES.GET_MV_DETAIL](state, mvDetail) {
    state.mvDetail = mvDetail;
  },
  // MV 点赞数
  [TYPES.GET_MV_LIKED_COUNT](state, likedCount) {
    state.mvLikedCount = likedCount;
  },
  // MV分享数
  [TYPES.GET_MV_SHARE_COUNT](state, shareCount) {
    state.mvShareCount = shareCount;
  },


  // 修改播放器显示状态
  [TYPES.SHOW_PLAYER](state, status) {
    state.showPlayer = status;
  },
  // // 修改 历史播放列表 显示状态
  // [TYPES.SHOW_HISTORY_PLAYERLIST](state, statue) {
  //   state.showHistoryPlaylist = statue;
  // },

  // 歌单信息
  [TYPES.GET_PLAYLIST_DETAIL](state, playlistDetail) {
    state.playlistDetail = playlistDetail;
  },
  // 歌单歌曲ID字符串
  [TYPES.GET_PLAYLIST_TRACKID](state, trackIds) {
    state.trackIds = trackIds;
  },
  // 歌单歌曲
  [TYPES.GET_PLAYLIST_TRACK](state, songs) {
    state.songs = songs;
  },
  // 相关歌单
  [TYPES.GET_RELATED_PLAYLIST](state, relatedPlaylist) {
    state.relatedPlaylist = relatedPlaylist;
  },
  // 歌单评论
  [TYPES.GET_PLAYLIST_COMMENTS](state, playlistComments) {
    state.playlistComments = playlistComments;
  },
  // 播放或者暂停
  [TYPES.PLAY_OR_PAUSE](state, statue) {
    state.playStatus = statue;
  },
  // 设置播放模式
  [TYPES.SET_PLAYMODE](state, playMode) {
    state.playMode = playMode;
  },
  // 设置静音
  [TYPES.SET_MUTE](state, mute) {
    state.isMuted = mute;
  },

  // 设置播放队列
  [TYPES.SET_PLAYING_QUEUE](state, track) {
    if (Array.isArray(track)) {
      //多首歌的 对象数组
      state.playingQueue.push(...track);
    } else {
      // 单首歌的对象
      state.playingQueue.push(track);
    }
  },
  //当前播放歌曲
  [TYPES.PLAYING_TRACK](state, track) {
    state.playingTrack = track;
  },
  // 历史播放歌曲列表
  [TYPES.SET_HISTORY_QUEUE](state, track) {
    if (!state.historyQueue.indexOf(track)) {
      state.historyQueue.push(track);
    }
  },
  // 显示历史播放列表
  [TYPES.SET_HISTORY_QUEUE](state, statue) {
    state.showHistoryQueue = statue;
  },


  /////////////////////////////////////////////////////////////
  // 待播放歌曲列表
  // [TYPES.ADD_TOBEPLAYED](state, tracks) {
  //   state.toBePlayed.push(...tracks);
  // },


  // 清空历史播放列表
  [TYPES.CLEARALL_HISTORY_PLAYLIST](state, emptyHistory) {
    state.historyPlaylist = emptyHistory;
  },
  // 删除某条播放历史记录
  [TYPES.REMOVE_FROM_HISTORY](state, trackIndex) {
    state.historyPlaylist.splice(trackIndex, 1);
  },

  // 切换歌曲 改索引
  [TYPES.SET_TRACK_INDEX](state, index) {
    state.playingIndex = index;
  },
}
export default mutations;
