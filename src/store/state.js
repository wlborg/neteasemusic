/*
 * @Author: ChaiHongJun
 * @Date: 2021-04-09 16:25:14
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-05-01 17:21:50
 * @version:
 * @Description:共享数据内容
 */
const state = {
  /* 首页 */
  // banner
  banner: [],
  // 推荐歌单
  personalized: [],
  // 推荐新歌
  newSongs: [],
  // 推荐歌手
  artists: [],

  /* 排行榜 */
  list: [],

  /*  歌单 */
  // 热门标签
  tags: [],
  // categories 歌单大分类
  categories: {},
  // sub 歌单小分类
  sub: [],
  // 精选歌单
  chosenPlaylist: [],
  // 精选歌单数量
  chosenPlaylistCount: 0,

  /* 歌手 */
  artistList: [],

  /* MV */
  mvList: [],
  mvSrc: "",
  //热门评论
  mvHotComments: [],
  // 最新评论
  mvComments: [],
  // mv 评论数
  mvCommentsCount: 0,
  // 相关mv
  simiMvs: [],
  // MV 详情
  mvDetail: {},
  //mv 点赞数
  mvLikedCount: 0,
  //MV 分享数
  mvShareCount: 0,


  // 播放器显示状态
  showPlayer: false,
  // 播放列表显示状态
  showHistoryQueue: false,
  // 歌单信息
  playlistDetail: {},
  // 歌单歌曲ID
  trackIds: [],
  // 歌单歌曲
  songs: [],
  // 相关歌单
  relatedPlaylist: {},
  // 歌单评论
  playlistComments: {},


  // 播放状态  true 播放 ，false 暂停
  playStatus: true,
  // 静音状态
  isMuted: false,
  //播放模式  (列表循环 0，单曲循环 1，列表随机 2)
  playMode: 0,

  //  播放队列
  playingQueue: [],

  //历史播放列表
  historyQueue: [],

  // 当前播放列表 歌曲索引
  playingIndex: 0,

  // 当前播放歌曲
  // playingTrack: {},
  //////////////////////////////////////








}
export default state;
