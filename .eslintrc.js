/*
 * @Author: ChaiHongJun
 * @Date: 2021-03-23 10:09:54
 * @LastEditors: ChaiHongJun
 * @LastEditTime: 2021-05-03 09:43:47
 * @version: 
 * @Description: 
 */
module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ["plugin:vue/essential", "eslint:recommended", "@vue/prettier"],
  parserOptions: {
    parser: "babel-eslint",
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-useless-escape": "off"
  },
};
