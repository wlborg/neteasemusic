# neteasemusic

照着 https://gitee.com/lxhcool/desktop-nicemusic 学习练习的代码
需要将后端代码也拖到本地才能配合好使用
后端的 API 需要运行起来(`node app.js`)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
